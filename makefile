MCU = atmega328p
PROG = usbasp
FREQ = 16000000UL
CC = avr-gcc
CFLAGS = -mmcu=$(MCU) -DF_CPU=$(FREQ) -Wl,-u,vfprintf -lprintf_flt -lm -I. -Os -mcall-prologues -Wall
OBJCOPY = avr-objcopy
OFLAGS = -j .text -j .data -O ihex
FLASHER = avrdude
FFLAGS = -p $(MCU) -c $(PROG) -U flash:w:

TARGET = gates
EXTS = usart.c safety.c stepper.c mytimer.c
DEPS = usart.o safety.o stepper.o mytimer.o

all: compile convert upload
bin: compile
build: compile convert
flash: upload
convert: compile

compile: $(TARGET).c
	$(CC) $(CFLAGS) -c $(EXTS)
	$(CC) $(CFLAGS) -o $(TARGET).out $(DEPS) $(TARGET).c
convert:
	$(OBJCOPY) $(OFLAGS) $(TARGET).out $(TARGET).hex
	rm -f *.out
upload:
	$(FLASHER) $(FFLAGS)$(TARGET).hex
	rm -f *.hex
clean:
	rm -f  $(TARGET).out *.hex
clear:
	rm -f *.o *.out *.hex
