#define F_CPU 16000000UL

#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdfix.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "usart.h"
#include "safety.h"
#include "stepper.h"
#include "mytimer.h"
/////////////////////////////////////////////////////////
//macros


//QOL macros
#define setb(port,pin)    port |= 1<<pin    //set bit
#define clrb(port,pin)    port &= ~(1<<pin) //clear bit
#define negb(port,pin)    port ^= 1<<pin    //negate bit
/////////////////////////////////////////////////////////
//Global vars
char tmp[100]; //auxiliary variable for (mainly) storing converted strings
volatile char iflag = -1; //interrupt fired flag
volatile int trg0 = 0;
volatile int trg1 = 0; //storing int trigger counts
volatile char isBlocked = 0; //indicate state
volatile unsigned long t;
volatile unsigned long t2;
unsigned long trgt = 0; //storing timestamps
//int trains = 0;
//int carriages = 0; //storing stats

/////////////////////////////////////////////////////////
//INTERRUPT VECTS
ISR(INT0_vect){
	if(trg0 == 0) t = mytime(); //first trigger timestamp
	if(iflag != 0) iflag = 0; //set int_fired flag
	trg0++; //count triggers
	t2 = mytime(); //timestamp each trigger

	/*
	//send debug info (comment out if not needed)
	sprintf(tmp,"trg0:%dflg:%d\r\n",trg0,iflag);
	USART_putstring(tmp);
	*/
}
ISR(INT1_vect){
	if(trg1 == 0) t = mytime(); //first trigger timestamp
	if(iflag != 1) iflag = 1; //set int_fired flag
	trg1++; //count triggers
	t2 = mytime(); //timestamp each trigger

	/*
	//send debug info (comment out if not needed)
	sprintf(tmp,"trg1:%dflg:%d\r\n",trg1,iflag);
	USART_putstring(tmp);
	*/
};
/////////////////////////////////////////////////////////
//FUNCTIONS
void initIO(void) {
    	//External interrupts init
    	EICRA=(1<<ISC11) | (1<<ISC10) | (1<<ISC01) | (1<<ISC00); //enable INT0 and INT1, mode: rising edge
    	EIMSK=(1<<INT1) | (1<<INT0); //enable interrupt vectors
    	EIFR=(1<<INTF1) | (1<<INTF0); //reset interrupt flags

	//set io pin directions
	//PORTC 0-3 OUT -- signalling LEDs and sound
	DDRC=0xF;
	//PORTB 1-5 OUT -- status LED and stepper controller
	DDRB=0x3E;
}

int main(void) {
	//executed once after every reset
	sei();	//globally enable interrupts
	initIO(); //initialize IO pors
	initTimer(); //initialize timers
    	initUSART(); //intialize USART
	//init_ok flash with all lights and buzzer
    	PORTC = 0xF;
    	_delay_ms(200);
    	PORTC = 0x00;
    	USART_putstring("Init_OK\r\nSTATS RESET!\r\n"); //send info about successful init

	//local vars
	char iflag_prev = -1;

	//infinite loop
	while (1) {
		switch(iflag){
			case 0:
				if(isBlocked == 0){
					isBlocked = 1; //change state to blocked
					iflag_prev = 0; //note which flag was set
					motor_step(50); //lower gate
					USART_putstring("Train passing from right...\r\n"); //send info
					trgt = t; //timestamp trigger time
				}
				if(isBlocked == 1 && iflag_prev == 1 && trg0 == trg1){
					isBlocked = 0;	//change state
					motor_step(-50); //raise gate
					eval(); //evaluate stats
					iflag = -1; //clear int fired flag
					trg0 = 0;
					trg1 = 0; //clear trigger counts
				}
				break;
			case 1:
				if(isBlocked == 0){
					isBlocked = 1;
					iflag_prev = 1;
					motor_step(50);
					USART_putstring("Train passing from left...\r\n");
					trgt = t;
				}
				if(isBlocked == 1 && iflag_prev == 0 && trg1 == trg0){
					isBlocked = 0;
					motor_step(-50);
					eval();
					iflag = -1;
					trg0 = 0;
					trg1 = 0;
				}
				break;
		};

		if(isBlocked == 1){
			light_blocked(1); //lights blinking red with(1)/without(0) sound
		}
		else {
			light_clear(); //lights blinking white, no sound
		};
	}
	return 0; //hopefully never reached
}
