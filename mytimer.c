#define F_CPU 16000000UL

#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdfix.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "mytimer.h"

volatile unsigned long ovf_cnt = 0;

void initTimer(void){
	//Timer/Counter 0 init
	TCNT0 = 0; //timer0 init with value 0
	TIMSK0 |= (1<<TOIE0); //enable ovf interrupt
	TCCR0B |= (1<<CS01)|(1<<CS00); //start timer0 with clk/64 prescaler
/*
	//Timer/Counter 1 init
	TCNT1 = 0; //initialize timer with value 0
	TCCR1B |= (1<<ICES1); //input capture on rising edge
	TIMSK1 |= (1<<ICIE1)|(1<<TOIE1); //enable input capture and ovf interrupts
	TCCR1B |= (1<<CS12)|(1<<CS10); //start timer with clk/1024 prescaler
*/
};

//Timer0 overflow interrupt vector
ISR(TIMER0_OVF_vect){
	ovf_cnt++; //increment number of ovfs
};

/*Timer1 currently unused
//Timer1 input capture vector
ISR(TIMER1_CAPT_vect){

};
//Timer1 ovf vector
ISR(TIMER1_OVF_vect){

};
*/

//returns microseconds elapsed from reset -- overflows after cca 70hrs
//ovf_cnt * 256 -- amount of ticks between ovfs
//+TCNT0 -- add actual timer value
//*4 -- step is ~4us/tick due to prescaling -- multiplication to get correct value in us from ticks
//!!!NOT GENERALIZED, WORKS ONLY WITH 16MHz SYSTEM CLOCK!!!
unsigned long mytime(){
	return((ovf_cnt * 256) + TCNT0) * 4;
};

accum ssinceboot(){
	return mytime() * 1e-6; //return current timestamp already in seconds
};
