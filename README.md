# xingcontrol

Firmware for a simple ATMega 328 mcu controlled model rail crossing.

Accomodates two optical gates for triggering, buzzer and three (or six) leds for signalization and a stepper controller to close/lift the barriers. 

Info about crossing state and number of trains and carriages is sent over serial.

Gimmicks include: non persistent stat keeping and crossing speed calculation in cm/s.

Not generalized, pin and port settings may need adjusting. 

Makefile is set up to use avrdude with usbASP programmer.
